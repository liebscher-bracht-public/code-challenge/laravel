# Laravel

Laravel envirnoment

## Getting started

Install Docker Desktop

- https://docs.docker.com/desktop/mac/install/
- https://docs.docker.com/desktop/windows/install/

## Usage

To start Docker environment, go to terminal in project folder.

### Start

    docker-compose up -d

### Stop 
    
    docker-compose down

## Install vendors and initialized laravel

    docker-compose exec php composer install \
    && docker-compose exec php cp .env.example .env \
    && docker-compose exec php php artisan key:generate

## Browser

    http://localhost

## Database usage

    host: mysql
    user: dbuser
    password: dbpassword
    port: 3306

## Mailhog

    http://localhost:8025

### SMTP
    
    host: mailhog
    user: <EMPTY>
    password: <EMPTY>
    port: 1025
    auth: plain